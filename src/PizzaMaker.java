import director.Director;
import pizza.Pizza;
import builder_pizza.PizzaBuilder;
import pizza.basic_elements_pizza.Cheese;
import pizza.basic_elements_pizza.Meat;
import pizza.basic_elements_pizza.Soys;
import pizza.basic_elements_pizza.Vegetables;


public class PizzaMaker {

    public static void main(String[] args) {

        Director director = new Director();

        PizzaBuilder margarita = new PizzaBuilder();
        director.constructMargiritaPizza(margarita);
        Pizza pizzaMargarita = margarita.cookingPizza();

        System.out.println(pizzaMargarita);
        margarita.setMeat(Meat.HAM);
        margarita.setAddMeat(Meat.HAM);
        margarita.setAddCheese(Cheese.RUSSIAN);
        margarita.setAddSoys(Soys.SWEET);

        PizzaBuilder pepperoni = new PizzaBuilder();
        director.constructPepperoniPizza(pepperoni);
        Pizza pizzaPepperoni = pepperoni.cookingPizza();

        System.out.println(pizzaPepperoni);
        pepperoni.setMeat(Meat.HAM);
        pepperoni.setVegetables(Vegetables.TOMATO);
        pepperoni.setAddVegetables(Vegetables.CHELI);
        pepperoni.setAddCheese(Cheese.ITALIAN);






    }

}
