package pizza.basic_elements_pizza;

public enum NamePizza {

    MARGARITA,
    KALCONE,
    PEPPERONI,
    MEXICO,
    KVADRO,
    AMERIKANO,
    KORRIDA,
    LYI,
    GAVAI

}
