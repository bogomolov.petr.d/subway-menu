package pizza.basic_elements_pizza;

public enum FormePizza {

    ROUND,
    SQUARE,
    OVAL

}
