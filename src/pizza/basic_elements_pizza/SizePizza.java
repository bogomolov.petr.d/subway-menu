package pizza.basic_elements_pizza;

public enum SizePizza {

    SMALL,
    MIDDLE,
    BIG

}
