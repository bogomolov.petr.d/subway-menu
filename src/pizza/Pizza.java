package pizza;

import pizza.basic_elements_pizza.*;

public class Pizza {

    private final NamePizza namePizza;
    private final FormePizza formePizza;
    private final SizePizza sizePizza;
    private final Cheese cheese;
    private final Soys soys;


    public Pizza(NamePizza namePizza, FormePizza formePizza, SizePizza sizePizza, Cheese cheese, Soys soys) {
        this.namePizza = namePizza;
        this.formePizza = formePizza;
        this.sizePizza = sizePizza;
        this.cheese = cheese;
        this.soys = soys;
    }

    @Override
    public String toString() {
        return "Pizza " + namePizza +
                ", formePizza-" + formePizza +
                ", sizePizza-" + sizePizza +
                ", cheese-" + cheese +
                ", soys-" + soys;
    }
}
