package builder_pizza;

import pizza.basic_elements_pizza.*;

public interface Build {

    /*
    назначение названия пиццы
     */

    void setName(NamePizza namePizza);

    /*
    назначаем размер пиццы
     */

    void setSize(SizePizza sizePizza);

    /*
    назначаем форму пиццы
     */

    void setForme(FormePizza formePizza);

    /*
    добавляем в пиццу сыр
     */

    void setCheese(Cheese cheese);

    /*
    добавляем соус
     */

    void setSoys(Soys soys);






}
