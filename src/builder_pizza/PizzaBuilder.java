package builder_pizza;

import pizza.Pizza;
import pizza.basic_elements_pizza.*;

public class PizzaBuilder implements Build {

    private NamePizza namePizza;
    private FormePizza formePizza;
    private SizePizza sizePizza;
    private Cheese cheese;
    private Soys soys;
    private Vegetables vegetables;
    private Meat meat;
    private Seafood seafood;


    @Override
    public void setName(NamePizza namePizza) {
        this.namePizza = namePizza;
    }

    @Override
    public void setSize(SizePizza sizePizza) {
        this.sizePizza = sizePizza;
    }

    @Override
    public void setForme(FormePizza formePizza) {
        this.formePizza = formePizza;
    }

    @Override
    public void setCheese(Cheese cheese) {
        this.cheese = cheese;
    }

    @Override
    public void setSoys(Soys soys) {
        this.soys = soys;
    }


    public void setVegetables(Vegetables vegetables) {
        this.vegetables = vegetables;
        System.out.println("+ " + vegetables);
    }


    public void setMeat(Meat meat) {
        this.meat = meat;
        System.out.println("+ " + meat);

    }

    public void setSeafood(Seafood seafood) {
        this.seafood = seafood;
        System.out.println("+ " + seafood);

    }

    public void setAddCheese(Cheese cheese) {
        this.cheese = cheese;
        System.out.println("+ extra cheese " + cheese);
    }

    public void setAddSoys(Soys soys) {
        this.soys = soys;
        System.out.println("+ extra soys " + soys);
    }

    public void setAddVegetables(Vegetables vegetables) {
        this.vegetables = vegetables;
        System.out.println("+ extra vegetables " + vegetables);
    }

    public void setAddMeat(Meat meat) {
        this.meat = meat;
        System.out.println("+ extra meat  " + meat);
    }

    public void setAddSeafood(Seafood seafood) {
        this.seafood = seafood;
        System.out.println("+ extra meat  " + seafood);
    }


    public Pizza cookingPizza() {
        return new Pizza(namePizza,formePizza,sizePizza, cheese, soys);
    }


}
