package director;

import pizza.basic_elements_pizza.*;
import builder_pizza.Build;

public class Director {

    public void constructMargiritaPizza(Build build) {

        build.setName(NamePizza.MARGARITA);
        build.setSize(SizePizza.MIDDLE);
        build.setForme(FormePizza.SQUARE);
        build.setCheese(Cheese.ITALIAN);
        build.setSoys(Soys.SWEET);


    }

    public void constructPepperoniPizza(Build build) {

        build.setName(NamePizza.PEPPERONI);
        build.setForme(FormePizza.OVAL);
        build.setSize(SizePizza.BIG);
        build.setCheese(Cheese.RUSSIAN);
        build.setSoys(Soys.ACUTE);

    }
}
